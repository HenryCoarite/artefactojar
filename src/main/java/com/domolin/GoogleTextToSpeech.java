/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domolin;
 
import java.io.InputStream; // import  classes from Java library
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import javazoom.jl.player.Player; // import class from jl1.0.1.jar
 
public class GoogleTextToSpeech {
  private static String ENCODING = "UTF-8"; //make constants and assign values to them
  private static String URL_BEGINNING = "http://translate.google.com/translate_tts?ie=";
  private static String URL_QUERY = "&q=";
  private static String URL_TL = "&tl=";
  private static String USER_AGENT_LITERAL = "User-Agent";
  private static String USER_AGENT = "Mozilla/4.7";
 
  public void say( String phrase, String lang ) {
 
    try {
      //Make full URL
      phrase=URLEncoder.encode(phrase, ENCODING); //assign value to variable with name 'phrase' by use method encode from class URLEncoder 
      String sURL = URL_BEGINNING + ENCODING + URL_TL + lang + URL_QUERY + phrase; //assign value to variable sURL 
          URL url = new URL(sURL); // make instance url with constructor
 
          //Create connection
          URLConnection urlConn = url.openConnection(); //assign value to variable urlConn 
          HttpURLConnection httpUrlConn = (HttpURLConnection) urlConn; //Declaring  httpUrlConn var of type HttpURLConnection, assigning it  value to  var urlConn (reduce to  HttpURLConnection)
          httpUrlConn.addRequestProperty(USER_AGENT_LITERAL, USER_AGENT);// use method 
 
          //Create stream
          InputStream mp3WebStream = urlConn.getInputStream();//create instance and assign it a value
 
          //Play stream
          Player plr = new Player(mp3WebStream); //create instance plr with constructor
          plr.play(); //use method
    }
      //use exception with name ex
      catch (Exception ex) { 
      ex.printStackTrace(); //use method
    }
  }
}