/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domolin;

import com.domolin.raspberry.BipUtil;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.GpioPinPwmOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.util.CommandArgumentParser;
import java.io.Console;

/*import com.pi4j.util.Console;*/
/**
 *
 * @author Henry Coarite
 */
public class TestLed {

    public static void main(String[] args) throws InterruptedException {

        BipUtil bip = new BipUtil(args);
        //util.startGPIOInstance();
        //Ojo
        byte[] eye = new byte[]{0x3c, 0x42, 0x42, 0x5a, 0x5a, 0x42, 0x42, 0x3c};
        byte[] blank = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

        final com.pi4j.util.Console console = new com.pi4j.util.Console();

        console.title("Control de motor lado izquierdo");
        console.promptForExit();
        bip.moveVehicleForward(100);
        console.println("Velocidad: " + bip.getLeftMotorControl().getPwm());
        console.println("Presione enter");
        System.console().readLine();
        bip.moveVehicleBack(80);
        console.println("Velocidad: " + bip.getLeftMotorControl().getPwm());

        System.console().readLine();
        bip.moveVehicleBack(50);
        console.println("Velocidad: " + bip.getLeftMotorControl().getPwm());

        console.println("Presione enter");
        System.console().readLine();

        // set the PWM rate to 0
        bip.getLeftMotorControl().setPwm(0);
        console.println("Velocidad: " + bip.getLeftMotorControl().getPwm());

        // stop all GPIO activity/threads by shutting down the GPIO controller
        // (this method will forcefully shutdown all GPIO monitoring threads and scheduled tasks)
        bip.getGpio().shutdown();
    }
}
