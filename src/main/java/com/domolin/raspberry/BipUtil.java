/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domolin.raspberry;

import com.domolin.raspberry.type.SpiPort;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.GpioPinPwmOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.spi.SpiChannel;
import com.pi4j.io.spi.SpiDevice;
import com.pi4j.io.spi.SpiFactory;
import com.pi4j.util.CommandArgumentParser;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Henry Coarite
 */
public class BipUtil extends RaspberryPi implements RaspberryPiImpl {

    private SpiDevice spi0;

    private GpioController gpio;

    private GpioPinPwmOutput leftMotorControl;

    private GpioPinPwmOutput pwmRight;

    /**
     * Pines de control para el lado izquierdo
     */
    private GpioPinDigitalOutput pin1;

    private GpioPinDigitalOutput pin2;

    public BipUtil(String[] args) {
        //spi0 = SpiFactory.getInstance(SpiChannel.CS0, SpiDevice.DEFAULT_SPI_SPEED, SpiDevice.DEFAULT_SPI_MODE);
        gpio = GpioFactory.getInstance();
        pin1 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_25);
        pin2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_29);
        pin1.setState(PinState.LOW);
        pin2.setState(PinState.HIGH);

        leftMotorControl = gpio.provisionPwmOutputPin(CommandArgumentParser.getPin(
                RaspiPin.class,
                RaspiPin.GPIO_24,
                args));
        com.pi4j.wiringpi.Gpio.pwmSetMode(com.pi4j.wiringpi.Gpio.PWM_MODE_MS);
        com.pi4j.wiringpi.Gpio.pwmSetRange(100);
        com.pi4j.wiringpi.Gpio.pwmSetClock(100);

    }

    public GpioPinPwmOutput getLeftMotorControl() {
        return leftMotorControl;
    }

    public GpioController getGpio() {
        return gpio;
    }

    @Override
    public void startGPIOInstance() {

    }

    /**
     * *
     * @param speed
     */
    public void moveVehicleForward(int speed) {
        pin1.setState(PinState.HIGH);
        pin2.setState(PinState.LOW);
        leftMotorControl.setPwm(speed);
    }
    
     public void moveVehicleBack(int speed) {
        pin1.setState(PinState.LOW);
        pin2.setState(PinState.HIGH);
        leftMotorControl.setPwm(speed);
    }

    public void stopIntance() {
        gpio.shutdown();
    }


    public void stopVehicle() {
        pin1.setState(PinState.LOW);
        pin2.setState(PinState.LOW);
        leftMotorControl.setPwm(0);
    }

    /**
     * CLK --> SCLK CS --> CE0 DIN --> MOSI
     *
     * @param image
     * @param spiPort
     */
    public void drawInMatrix8x8(byte[] image, Integer spiPort) {
        for (int i = 0; i < 8; i++) {
            try {
                if (spiPort.equals(SpiPort.ZERO.getCode())) {
                    spi0.write((byte) (i + 1), image[i]);
                }
            } catch (IOException ex) {
                Logger.getLogger(BipUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
