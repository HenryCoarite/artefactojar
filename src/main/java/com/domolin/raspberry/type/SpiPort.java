/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domolin.raspberry.type;

/**
 *
 * @author Henry Coarite
 */
public enum SpiPort {
    ZERO(Integer.valueOf(0), "Port Zero"),ONE(Integer.valueOf(0), "Port One");

    private Integer code;
    private String desc;

    private SpiPort(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public static SpiPort getZERO() {
        return ZERO;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
